#!/usr/bin/perl

use strict;
use warnings;
use Net::Pcap;
use NetPacket::Ethernet qw(:strip);
use NetPacket::IP;

my $file = $ARGV[0];
my $err = '';
my $count = 1;
my $start = undef;


if($file) {
    
    #read
    my $pcap = pcap_open_offline($file, \$err)
    or die "Unable to read $file : $err\n";
    
    if($ARGV[1]){
        open (FILE,'>', $ARGV[1]);
    } else {
        open (FILE, '>', 'output.txt');
    }

    #loop
    pcap_loop($pcap, -1, \&process_packet, '');

    #close
    pcap_close($pcap);
    close (FILE);
    
} else {
    die "Please specify a file. $err\n";
}

sub process_packet {
    
    my ($user_data, $header, $packet) = @_;
    
	for(my $j = 0; $j < 60; $j++) {
        print FILE "-";
	}
	print FILE "\n\n";
    
    #packet number
	print FILE "No. ".$count++."\n";
	
	#timestamp
	if($count == 2) {
        $start = "$header->{tv_sec}.$header->{tv_usec}";
    }
	my $time = "$header->{tv_sec}.$header->{tv_usec}";
	$time -= $start;
	printf FILE ("Timestamp: %.6f, ", $time);
	
	#length
    my $len = $header->{len};
   	print FILE "Length: $len bytes (".($len * 8)." bits) \n";
    
	#source and destination
	my $ip_obj = NetPacket::IP->decode(eth_strip($packet));
	print FILE "Src: $ip_obj->{src_ip}, Dest: $ip_obj->{dest_ip}, Protocol: $ip_obj->{proto}\n\n";
	
	#Hex|ASCII format
	my $i=0;
  	do {
        my $lg = substr $packet, $i, 16;
        printf FILE "%.8X : ", $i;
        $i+=16;
        print FILE unpack ('H2'x16, $lg), '  'x(16-length $lg);
        $lg =~ s/[\x00-\x1F\xFF]/./g;
        print FILE " $lg\n";
 	} until $i>=$len;
 	print FILE "\n";
}
